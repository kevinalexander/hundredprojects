REVERSE A STRING

Author:   Kevin Alexander
Date:     21 June 2019

Description:
    Enter a string and the program will reverse it and print it out.

