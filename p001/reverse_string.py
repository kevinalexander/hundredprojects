# reverse_string.py
#
# Author:   Kevin Alexander
# Date:     21 June 2019

def reverse_string(string):
    """Reverses the order of the given string."""

    new_string = ""

    for i in range(len(string)):
        new_string += string[-1 - i]
    
    return new_string

def print_reverse_string(string):
    """Prints the reverse ordering of the given string."""

    print(reverse_string(string))

if __name__ == '__main__':

    print("+Welcome to Reverse-a-String!+")
    print("If you wish to quit, simply type 'I want to quit.'")

    while(True):
        string = input("Please enter a string and I'll reverse it: ")

        if string.lower() == "i want to quit.":
            break

        print_reverse_string(string)

