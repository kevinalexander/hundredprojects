# piglatin.py
#
# Author:   Kevin Alexander
# Date:     23 June 2019

def trans_to_pl(english):
    """Translates an English word to that of Pig Latin."""
    pig_latin = ""
    
    for index in range(len(english)):
        if english[index].lower() not in 'aeiou':
            pig_latin = pig_latin + english[index]
        else:
            if pig_latin:
                pig_latin = english[index:] + pig_latin + "ay"
                break
            else:
                pig_latin = english + "ay"
                break
        index = index + 1
    return pig_latin

def trans_from_file(filename):
    """Translates a file in english and prints the contents in pig latin."""

    translation = ""
    with open(filename) as f:
       for line in f:
           for word in line.split():
               translation = translation + " " + trans_to_pl(word)
    print(translation.lstrip())
    return

def trans_from_prompt():
    """Translates an english word or sentence supplied by the user."""
    
    print("Enter a phrase and it will be translated from English to " +
          "Pig Latin.")
    print("Enter 'quit()' at anytime to quit.")

    while(True):
        line = input("Please enter a word or sentence: ")
        if line == 'quit()':
            break
        translation = ""
        for word in line.split():
            translation = translation + " " + trans_to_pl(word)
        print(translation.lstrip())
    return

def main():
    import sys
    
    sentence = ""

    print("PIG LATIN TRANSLATOR")

    if len(sys.argv) > 1:
        print("From file '" + sys.argv[1] + "':")
        trans_from_file(sys.argv[1])
    else:
        trans_from_prompt()

if __name__ == '__main__':
    main()
