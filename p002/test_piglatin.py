# test_piglatin.py
#
# Author:   Kevin Alexander
# Date:     26 June 2019

import piglatin

def test_piglatin():
    """Test the function trans_to_pl() with a number of different english words."""
    test_success = 0

    cons_words  = ["pig",
                   "latin",
                   "banana",
                   "happy",
                   "duck",
                   "me",
                   "too",
                   "bagel",]

    ccons_words = ["smile",
                   "string",
                   "stupid",
                   "glove",
                   "trash",
                   "floor",
                   "store",]

    vowel_words = ["eat",
                   "omelet",
                   "are",
                   "egg",
                   "explain",
                   "always",
                   "ends",
                   "honest",
                   "I",]
    
    cons_words_ans  = ["igpay",
                       "atinlay",
                       "ananabay",
                       "appyhay",
                       "uckday",
                       "emay",
                       "ootay",
                       "agelbay",]

    ccons_words_ans  = ["ilesmay",
                        "ingstray",
                        "upidstay",
                        "oveglay",
                        "ashtray",
                        "oorflay",
                        "orestay",]

    vowel_words_ans  = ["eatay",
                        "omeletay",
                        "areay",
                        "eggay",
                        "explainay",
                        "alwaysay",
                        "endsay",
                        "onesthay",
                        "Iay",]

    for index in range(len(cons_words)):
        if piglatin.trans_to_pl(cons_words[index]) == cons_words_ans[index]:
            test_success = test_success + 1
        index = index + 1

    print("Single consonant word tests: " + str(test_success) +
          " of " + str(len(cons_words_ans)) + " cases passed.")

    test_success = 0

    for index in range(len(ccons_words)):
        if piglatin.trans_to_pl(ccons_words[index]) == ccons_words_ans[index]:
            test_success = test_success + 1
        index = index + 1

    print("Clustered consonant word tests: " + str(test_success) +
          " of " + str(len(ccons_words_ans)) + " cases passed.")

    test_success = 0

    for index in range(len(vowel_words)):
        if piglatin.trans_to_pl(vowel_words[index]) == vowel_words_ans[index]:
            test_success = test_success + 1
        index = index + 1

    print("Vowel word tests: " + str(test_success) +
          " of " + str(len(vowel_words_ans)) + " cases passed.")

test_piglatin()
