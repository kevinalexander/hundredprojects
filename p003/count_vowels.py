#!/usr/bin/env python3
#
# count_vowels.py
#
# Author:   Kevin Alexander
# Date:     27 June 2019

def main():
    """Main function of program."""
    import sys

    print("VOWEL COUNTER")
    
    vowels = {'a': 0,
              'e': 0,
              'i': 0,
              'o': 0,
              'u': 0,}
    sentence = []

    if len(sys.argv) > 1:
        with open(sys.argv[1]) as f:
            lines = f.readlines()
        for line in lines:
            for word in line:
                sentence.append(word)
        print("From file '" + sys.argv[1] + "':")
    else:
        sentence = input("Please enter a sentence to be parsed: ").split()

    for word in sentence:
        for letter in word:
            if letter.lower() in 'aeiou':
                vowels[letter.lower()] = vowels[letter.lower()] + 1

    print("Total vowels: " + str(
          sum([count for count in vowels.values()])))
    for key, value in vowels.items():
        print("Vowel [" + key + "]: " + str(value))


if __name__ == '__main__':
    main()
